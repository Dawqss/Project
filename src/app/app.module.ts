import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RecipesModule} from './recipes/recipes.module';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HelloComponent} from './hello/hello.component';
import {ShoppingListService} from './shoppingList/shopping-list.service';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {RecipeService} from './recipes/recipe.service';
import {HttpModule} from '@angular/http';
import {DataStorageService} from './shared/data-storage.service';
import {AuthService} from './auth/auth.service';
import {AuthGuardService} from './auth/auth-guard.service';
import {ShoppingListModule} from './shoppingList/shopping-list.module';
import {AuthModule} from './auth/auth.module';
import {SharedModule} from './shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HelloComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AuthModule,
    SharedModule,
    ShoppingListModule
  ],
  providers: [
    ShoppingListService,
    RecipeService,
    DataStorageService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
