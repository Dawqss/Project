import {EventEmitter, Injectable} from '@angular/core';
import { Recipe } from "./recipe.model";
import {Ingredient} from "../shared/ingredient.model";
import {ShoppingListService} from "../shoppingList/shopping-list.service";
import {Subject} from "rxjs/Subject";

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe(
        'Spaghetti',
        'rodzaj długiego, cienkiego makaronu',
        'http://food.fnr.sndimg.com/content/dam/images/food/video/0/01/018/0186/0186701.jpg.rend.hgtvcom.616.462.suffix/1481335396438.jpeg',
        [
            new Ingredient('Meatballs', 10),
            new Ingredient('Noodle', 100),
            new Ingredient('Tomatoes', 3),
            new Ingredient('Spices', 10),
            new Ingredient('Salt', 2),
            new Ingredient('Pepper', 2),
        ]),
    new Recipe(
        'Hamburgier',
        'Krówka robi muu',
        'https://www.kingsford.com/wp-content/uploads/2014/11/kfd-howtohamburger-Burgers_5_0391-1024x621.jpg',
        [
            new Ingredient('Cow', 1),
            new Ingredient('Bread', 1),
            new Ingredient('Salad', 1),
            new Ingredient('Tomatoes', 1),
            new Ingredient('Onion', 1),
            new Ingredient('BBQ Sauce', 1),
            new Ingredient('Cheese', 1),
        ]
    )
  ];

  constructor(private slService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice())
  }


  getRecipes() {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList (ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }


  getRecipe(id: number) {
      return this.recipes[id];
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice())
  }

  updateRecipe(index:number , recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipesChanged.next(this.recipes.slice())
  }

  deleteRecipe(index:number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }


}
