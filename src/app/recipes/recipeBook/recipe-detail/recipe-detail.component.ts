import { Component, OnInit} from '@angular/core';
import { Recipe } from '../../recipe.model'
import {RecipeService} from "../../recipe.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
    selector: 'app-recipe-detail',
    templateUrl: './recipe-detail.component.html',
    styles: []
})


export class RecipeDetailComponent implements OnInit {

    recipe: Recipe;

    id: number;

    constructor(private rService: RecipeService,
                private route: ActivatedRoute,
                private router: Router
    ) {}

    ngOnInit() {
        this.route.params
            .subscribe(
            (params: Params) => {
                this.id = +params['id'];
                console.log(this.id);
                this.recipe = this.rService.getRecipe(this.id)
            }
        );
    }

    onAddToShoppingList() {
        this.rService.addIngredientsToShoppingList(this.recipe.ingredients);
    }


    onEditRecipe() {
        this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route})
    }

    onDeleteRecipe() {
        console.log(this.id);
        this.rService.deleteRecipe(this.id);
        this.router.navigate(['../'], {relativeTo: this.route})
    }
}
