import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'hello';

  ngOnInit() {
    firebase.initializeApp(
      {
        apiKey: 'AIzaSyCUW2nbPFXiTITXAK89Rq6FbWgXGwOsc38',
        authDomain: 'ng-recipe-book-b72c9.firebaseapp.com'
      }
    )
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
