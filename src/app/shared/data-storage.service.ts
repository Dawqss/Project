import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {RecipeService} from "../recipes/recipe.service";
import {Recipe} from "../recipes/recipe.model";
import 'rxjs/add/operator/map';
import {AuthService} from "../auth/auth.service";
@Injectable()
export class DataStorageService {
  constructor(private http: Http,
              private recipeServices: RecipeService,
              private authService: AuthService) {
  }


  storeRecipes() {
    const token = this.authService.getIdToken();
    return this.http.put('https://ng-recipe-book-b72c9.firebaseio.com/recipes.json?auth=' + token, this.recipeServices.getRecipes());
  }


  getRecipes() {
    const token = this.authService.getIdToken();
    this.http.get('https://ng-recipe-book-b72c9.firebaseio.com/recipes.json?auth=' + token)
      .map(
        (response: Response) => {
          const recipes: Recipe[] = response.json();
          for (let item of recipes) {
            if (!item['ingredient']) {
              item['ingredient'] = [];
            }
          }
          return recipes;
        }
      )
      .subscribe((recipes: Recipe[]) => {
        this.recipeServices.setRecipes(recipes);
      });
  }
}

