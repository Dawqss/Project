import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ShopListComponent} from './shop-list/shop-list.component';
import {ShopListEditComponent} from './shop-list-edit/shop-list-edit.component';

@NgModule({
  declarations: [
    ShopListComponent,
    ShopListEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})

export class ShoppingListModule {}