import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {ShoppingListService} from "../shopping-list.service";
import {NgForm} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {Ingredient} from "../../shared/ingredient.model";

@Component({
  selector: 'app-shop-list-edit',
  templateUrl: './shop-list-edit.component.html',
  styleUrls: ['./shop-list-edit.component.css']
})
export class ShopListEditComponent implements OnInit, OnDestroy {
  editMode = false;
  editedItemIndex: number;
  subscription: Subscription;
  subscriptionStatus: Subscription;
  editedItem: Ingredient;
  @ViewChild('f') shoppingListForm: NgForm;


  constructor(private shoppingListService: ShoppingListService) {}

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing
      .subscribe((id: number) => {
        this.editMode = true;
        this.editedItemIndex = id;
        this.editedItem = this.shoppingListService.getIngredient(id);
        this.shoppingListForm.setValue({
          name: this.editedItem.name,
          quanity: this.editedItem.quanity
        })
      });

    this.subscriptionStatus = this.shoppingListForm.valueChanges.subscribe((value: Ingredient) => {
      if (this.editMode) {
        this.shoppingListService.updateIngredient(this.editedItemIndex, {
          name: value.name,
          quanity: value.quanity
        })
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onClear() {
    this.editMode = false;
    this.shoppingListForm.reset()
  }

  onDelete() {
    if (this.editMode) {
      this.shoppingListService.deleteIngredients(this.editedItemIndex);
    }
    this.editMode = false;
    this.shoppingListForm.reset();
  }

  onAddItem(form: NgForm) {
    const value = form.value;
    const newIngredient = new Ingredient(value.name, value.quanity);
    if (this.editMode) {
      this.shoppingListService.updateIngredient(this.editedItemIndex, newIngredient);
      this.editMode = false;
      form.reset();
    } else {
      this.shoppingListService.addIngredient(newIngredient);
    }
  }
}
