import {RouterModule, Routes} from '@angular/router';
import {ShopListComponent} from './shoppingList/shop-list/shop-list.component';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
    {path: '', redirectTo:  '/recipes',  pathMatch: 'full'},
    {path: 'recipes', loadChildren: './recipes/recipes.module#RecipesModule'},
    {path: 'shopping-list', component: ShopListComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {}
